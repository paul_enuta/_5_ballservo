#include <EEPROM.h>
#include "Wire.h"
#include "LiquidCrystal.h"
#include "M2tk.h"
#include "utility/m2ghlc.h"

M2_EXTERN_ALIGN(top_menu);
uint8_t T1 = 1;
uint8_t T2 = 2;
uint8_t T3 = 3;
uint8_t T4 = 4;
uint8_t T5 = 5;

M2_LABEL(el_l0, NULL, "Setup:");
M2_LABEL(el_l1, NULL, "T 1 =");
M2_U8NUM(el_u1, "c2", 0, 99, &T1);
M2_LABEL(el_l2, NULL, "T 2 =");
M2_U8NUM(el_u2, "c2", 0, 99, &T2);
M2_LABEL(el_l3, NULL, "T 3 =");
M2_U8NUM(el_u3, "c2", 0, 99, &T3);
M2_LABEL(el_l4, NULL, "T 4 =");
M2_U8NUM(el_u4, "c2", 0, 99, &T4);
M2_LABEL(el_l5, NULL, "T 5 =");
M2_U8NUM(el_u5, "c2", 0, 99, &T5);

M2_SPACE(el_sp, "w1h1");
M2_LIST(list) = { &el_l0, &el_sp, &el_sp, &el_sp, &el_sp,
                  &el_l1, &el_u1, &el_sp, &el_l2, &el_u2, 
                  &el_l3, &el_u3, &el_sp, &el_l4, &el_u4,
                  &el_l5, &el_u5 };
M2_GRIDLIST(el_gridlist, "c5", list);
M2_ALIGN(top_menu, "-1|2W64H64", &el_gridlist);

// m2 object and constructor
// Note: Use the "m2_eh_4bd" handler, which fits better to the "m2_es_arduino_rotary_encoder" 
M2tk m2(&top_menu, m2_es_arduino_rotary_encoder, m2_eh_4bd, m2_gh_lc);

LiquidCrystal lcd(0);
const int buttonPin1 = A0;
const int buttonPin2 = A1;
const int buttonPin3 = A2;
const int buttonPin4 = A3;
const int buttonPin5 = A4;
const int setupPin = 4;
int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;
int buttonState5 = 0;
int setupState = 0;
volatile long Lei=0;
//volatile int T1=1;
//volatile int T2=2;
//volatile int T3=3;
//volatile int T4=4;
//volatile int T5=5;

void setup()
{
  m2_SetLiquidCrystal(&lcd, 20, 4);
  m2.setPin(M2_KEY_SELECT, A5);
  m2.setPin(M2_KEY_ROT_ENC_A, 10);
  m2.setPin(M2_KEY_ROT_ENC_B, 11);
  
  Serial1.begin(9600);
    while (!Serial1)
      {;} // wait for serial port to connect. Leonardo only
  //Serial1.println("Serial CDC started!");
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);
  pinMode(buttonPin4, INPUT);
  pinMode(buttonPin5, INPUT);
  pinMode(setupPin, INPUT);
  lcd.begin(20, 4);
  lcd.setBacklight(HIGH);
  lcd.clear();
  lcd.print(F("< MagicBall Select >"));
  EICRB |= (0<<ISC60)|(1<<ISC61);  // sets the interrupt type for EICRB (INT6)
  EIMSK |= (1<<INT6);              // activates the interrupt. 6 for 6, etc
/*
                                  EICRA  sets interrupt type for INT0...6
                              ISCn0  ISCn1 Where n is the interrupt. 0 for 0, etc
                                0      0   Triggers on low level
                                1      0   Triggers on edge
                                0      1   Triggers on falling edge
                                1      1   Triggers on rising edge
*/
}

void loop()
{
    lcd.setCursor(0, 1);
    lcd.print("Credit =");
    lcd.setCursor(9, 1);
    credit();
    lcd.setCursor(0, 2);
    lcd.print(" T 1|T 2|T 3|T 4|T 5");
    lcd.setCursor(0, 3);
    lcd.print("    |   |   |   |   ");

    // check if the pushbutton is pressed.
    buttonState1 = digitalRead(buttonPin1);
    buttonState2 = digitalRead(buttonPin2);
    buttonState3 = digitalRead(buttonPin3);
    buttonState4 = digitalRead(buttonPin4);
    buttonState5 = digitalRead(buttonPin5);
    setupState   = digitalRead(setupPin);

    if (buttonState1 == LOW & Lei>=T1) {     
      Serial1.println("X");
      Lei = Lei - T1;
      lcd.setCursor(2, 3);
      lcd.print("*");
      delay(1000);
      lcd.setCursor(2, 3);
      lcd.print(" ");  
      } 
        else {         }
    if (buttonState2 == LOW & Lei>=T2) {     
      Serial1.println("Z");
      Lei = Lei - T2;
      lcd.setCursor(6, 3);
      lcd.print("*");
      delay(1000);
      lcd.setCursor(6, 3);
      lcd.print(" ");  
      }   
        else {        }
    if (buttonState3 == LOW & Lei>=T3) {     
      Serial1.println("Y");
      Lei = Lei - T3;
      lcd.setCursor(10, 3);
      lcd.print("*");
      delay(1000);
      lcd.setCursor(10, 3);
      lcd.print(" ");  
      }   
        else {        }
    if (buttonState4 == LOW & Lei>=T4) {     
      Serial1.println("W");
      Lei = Lei - T4;
      lcd.setCursor(14, 3);
      lcd.print("*");
      delay(1000);
      lcd.setCursor(14, 3);
      lcd.print(" ");  
      }   
        else {        }
    if (buttonState5 == LOW & Lei>=T5) {     
      Serial1.println("V");
      Lei = Lei - T5;
      lcd.setCursor(18, 3);
      lcd.print("*");
      delay(1000);
      lcd.setCursor(18, 3);
      lcd.print(" ");  
      }   
        else {        }
    while (setupState == LOW) {     
      Serial1.println("Setup enabled.");
      menuloop();
      }
}

void menuloop (){
  while (setupState == LOW) {
          m2.checkKey();
          if ( m2.handleKey() )
          m2.draw();
          m2.checkKey();
          setupState = digitalRead(setupPin);}
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F("< MagicBall Select >"));
  setupState = digitalRead(setupPin);
}

ISR(INT6_vect){
  Lei ++;
}

void credit() {
      if (Lei<10)
     { 
       lcd.print(F(" "));
       lcd.print(Lei),lcd.print(F(" RON"));
     }
       else
       { 
         lcd.print(Lei),lcd.print(F(" RON"));
       }
     }

